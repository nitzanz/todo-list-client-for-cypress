const todos = [
    {listName: 'default', id: 'fio4nu34fnn32okmwiedci', 
        finishDate : 'default', task: 'new todo1', completed: false, shown: true},
    {listName: 'default', id: 'fio23onrfoi23hfdo9dfuedci', 
        finishDate : 'default', task: 'new todo2', completed: false, shown: true},
        
]

export default {
    listNames: ['default'], 
    shownListName: 'default', 
    todos
}