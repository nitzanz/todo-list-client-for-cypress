import { gql } from 'apollo-boost';

export class CommunicationHelpers {
    constructor(client){
        this.client = client;
    }

    async fetchMutation(mutation, variables = {}) {
        try {
        let respJson = await this.client.mutate(toMutationJson(mutation.body, variables));
        return await respJson.data[mutation.name];
        } catch (err) {
            console.error(err);
            return false;
        }
    }

    async fetchQuery(query, variables = {}) {
        try {
            let respJson = await this.client.query(toQueryJson(query.body, variables));
            return await respJson.data[query.name];
        } catch (err) {
            console.error(err);
            return false;
        }
    }
}

export const exportQuery = (queryBody, queryName) => ({
    body: queryBody,
    name: queryName
})

function toQueryJson(string, variables = {}) {
    return { query: gql(string), variables: variables };
}

function toMutationJson(string, variables) {
    return { mutation: gql(string), variables: variables };
}
