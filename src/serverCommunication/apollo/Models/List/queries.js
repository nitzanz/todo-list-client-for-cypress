import {exportQuery} from '../helpers';

const queries = {
    getLists : exportQuery(`
        query lists {
            Lists
        }
    `, 'Lists'),
    addList: exportQuery(`
        mutation AddList($listName: String!) {
            add_list(listName: $listName)
        }
    `, 'add_list'),
    deleteList: exportQuery(`
        mutation DeleteList($listName: String!) {
            delete_list(listName: $listName)
        }
    `, 'delete_list')   
}

export default queries;