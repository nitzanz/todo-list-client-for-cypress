import axios from 'axios';

const initialize = (client) => {
    return {
        getTodo: async (id) => {
            let response = await axios.get(`${client.link}/todo/${id}`)
            return response.data;
        },
        getAllTodos: async () => {
            let response = await axios.get(`${client.link}/todo`);
            return response.data;
        },
        addTodo: async (todo) => {
            delete todo.finishDate
            let response = await axios.post(`${client.link}/todo`, todo);
            return response.data;
        },
        editTodoTask: async (task, id) => {
            let response = await axios.put(`${client.link}/todo/${id}`, task)
            return response.data;
        },
        deleteTodo: async (id) => {
            let response = await axios.delete(`${client.link}/todo/${id}`)
            return response.data;
        },
        updateFinishDate: async (finishDate, id) => {
            let response = await axios.put(`${client.link}/todo/date/${id}`, {finishDate})
            return response.data;
        },
        removeFinishDate: async (id) => {
            let response = await axios.delete(`${client.link}/todo/date/${id}`)
            return response.data;
        },
        toggleTodo: async (id) => {
            let response = await axios.put(`${client.link}/todo/toggle/${id}`);
            return response.data.isCompleted;
        },
        getLists: async () => {
            let response = await axios.get(`${client.link}/todo-list/`);
            return response.data;
        },
        addList: async (listName) => {
            let response = await axios.post(`${client.link}/todo-list/`, {name: listName});
            return response.data;
        },
        deleteList: async (listId) => {
            let response = await axios.delete(`${client.link}/todo-list/${listId}`)
            return response.data;
        }
    }
}

export default initialize;
