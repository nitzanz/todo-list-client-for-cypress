import { connect } from 'react-redux'
import TodoListSideBar from './presentational/TodoListSideBar';

const mapStateToProps = state => ({
    listNames : state.listNames
});

const mapDispatchToProps = (dispatch, {actions, anchor}) => ({
    showTodoList: listToShow => dispatch(actions.showTodoList(listToShow)),
    removeTodoList: listToRemove => dispatch(actions.removeTodoList(listToRemove))
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoListSideBar);