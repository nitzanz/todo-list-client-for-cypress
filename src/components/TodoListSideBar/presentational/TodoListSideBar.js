import React, {useState} from 'react';
import clsx from 'clsx';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import useStyles from '../../../styles/TodoListSideBarStyles';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import RemoveListAlert from './RemoveListAlert';

export default function TodoListSideBar({anchor, showTodoList, removeTodoList, listNames}) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [todoListToRemove, setTodoListToRemove] = useState('');

  const checkIfLast = (listDetails)=>{
      return (listNames.length !== 1 &&
          <ListItemIcon data-test={'removeIcon ' + listDetails.name} onClick={() => {
          setTodoListToRemove(listDetails);
          setDialogOpen(true);
      }}>
          <RemoveCircleIcon/>
      </ListItemIcon>);
  }

  const toggleDrawer = (open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    
    setOpen(open);
  };

  const list = (anchor) => (
    <div data-test='MainContainer'
      className={clsx(classes.list, {
        [classes.fullList]: anchor === 'top' || anchor === 'bottom',
      })}
      role="presentation"
      onKeyDown={toggleDrawer(false)}
    >
      <List>
        {listNames.map((listDetails, index) => (
          <ListItem data-test={'list ' + listDetails.name} button key={listDetails.name} onClick={() => showTodoList(listDetails)}>
            <ListItemText primary={listDetails.name} />
              {checkIfLast(listDetails)}
          </ListItem>
        ))}
      </List>
    </div>
  );

  return (
    <div>
        <React.Fragment>
            <Button data-test='openButton' className={clsx(classes.showButton)} onClick={toggleDrawer(true)}>Show Todo Lists</Button>
            <Drawer anchor={anchor} open={open} onClose={toggleDrawer(false)}>
            {list(anchor)}
            </Drawer>
            <RemoveListAlert todoListToRemove={todoListToRemove.name} removeTodoList={() => removeTodoList(todoListToRemove)} open={dialogOpen} setOpen={setDialogOpen}/>
        </React.Fragment>
    </div>
  );
}
