import React from 'react';
import { Provider } from 'react-redux';
import setUpRedux from '../helpers/setUpRedux';
import TodoFormContainer from './TodoForm/TodoFormContainer';
import TodoListContainer from './TodoList/TodoListContainer';
import TodoListSideBarContainer from './TodoListSideBar/TodoListSideBarContainer';
import TodoListFormContainer from './TodoListForm/TodoListFormContainer';
import AsyncCreators from '../actions/AsyncCreators';
import * as syncActions from '../actions/SyncCreators';
import RestCommunicator from '../serverCommunication/rest/RestServerCommunicator';

//initializing
let communicator = RestCommunicator;
let actions = new AsyncCreators(communicator);
let store = setUpRedux(communicator, false);
//
function TodoApp() {
  return (
    <Provider store={store}>   
      <div style ={{
        position: 'absolute',
        left: '20px',
        top: '20px',
      }}>
        <TodoListSideBarContainer actions={actions} anchor='left'/>
        <TodoListFormContainer actions={actions}/>
      </div>
      <TodoFormContainer actions={actions}/>
      <TodoListContainer actions={actions}/>
    </Provider>
  );
}

export default TodoApp;
