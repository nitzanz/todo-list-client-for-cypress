import 'date-fns';
import React, {useState, useCallback} from 'react';
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import Button from '@material-ui/core/Button';

export default function FinishDate({finishDate, id, updateFinishDate, removeFinishDate}) {
  if(finishDate === 'default'){
    finishDate = '2020-08-18';
  }

  if(typeof(id) === 'object'){
    id = id.key;
  }

  const [selectedDate, setSelectedDate] = useState(new Date(finishDate));
  
  const handleDateChange = useCallback(date => {
    setSelectedDate(date);
    updateFinishDate(date.toDateString(), id);
  }, []);


  return (
    <div style={{display: 'flex', flexDirection: 'row'}}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Grid container justify="space-around">
                <KeyboardDatePicker data-test='Calendar'
                disableToolbar
                variant="inline"
                format="dd/MM/yyyy"
                margin="normal"
                id="date-picker-inline"
                label="Date picker inline"
                value={selectedDate}
                onChange={handleDateChange}
                KeyboardButtonProps={{
                    'aria-label': 'change date',
                }}
                />
            </Grid>
        </MuiPickersUtilsProvider>
        <Button data-test='removeDateButton' 
          onClick={() => {
          setSelectedDate(new Date('2020-08-18')); 
          removeFinishDate(id)
        }}>
          Remove Finish Date
        </Button>
    </div>
  );
}
