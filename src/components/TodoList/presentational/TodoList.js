import React from 'react';
import Todo from './Todo';

function TodoList({
  shownListName, 
  id,
  todos, 
  toggleTodo, 
  removeTodo, 
  editTodo, 
  updateFinishDate, 
  removeFinishDate
}) {
  return (
    <div>
      Current List Name : {shownListName.name}
    <ul data-test='TodoContainer' style={{ paddingLeft: 10, width: '95%' }}>
      {todos.map(todo => {
        let finishDate = todo.finishDate ?? 'default';
        let parsedTodo = Object.assign({}, todo);
        if(parsedTodo.finishDate){
          delete parsedTodo.finishDate;
        }
        if(todo){
          return (<Todo data-test='Todo' toggleTodo={() => toggleTodo(todo.id)} removeTodo={removeTodo} editTodo={editTodo}
            updateFinishDate={updateFinishDate} removeFinishDate={removeFinishDate} key={todo.id} {...parsedTodo} finishDate={finishDate} />)
        }
      })}
    </ul>
    </div>
  );
}

export default TodoList;
